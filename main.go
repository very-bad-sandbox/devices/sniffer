package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"net"
	"strconv"
	"strings"

	"github.com/jacobsa/go-serial/serial"
	"gitlab.tools.russianpost.ru/eas-platform/devices/utils"
	"gitlab.tools.russianpost.ru/eas-platform/go-infrastructure/logging"
)

var port = flag.Int("port", 7890, "Listen tcp port")
var deviceComPort = flag.String("com", "COM3", "Connect to device com port")

func init() {
	flag.Parse()
}

func main() {
	device, err := serial.Open(serial.OpenOptions{
		PortName:        *deviceComPort,
		BaudRate:        115200,
		DataBits:        8,
		StopBits:        1,
		MinimumReadSize: 1,
	})
	if err != nil {
		log.Fatal(err)
	}

	listener, err := net.Listen("tcp", "127.0.0.1:"+strconv.Itoa(*port))
	if err != nil {
		log.Fatal(err)
	}

	TestDriverConn, err := listener.Accept()
	if err != nil {
		log.Fatal(err)
	}

	go func() {
		buf := make([]byte, 32*1024)
		for {
			nr, er := TestDriverConn.Read(buf)
			if nr > 0 {
				packets := parseBySTX(buf[:nr])
				for _, p := range packets {
					log.Println("////////////////////////////////////////REQUEST////////////////////////////////////////")
					log.Printf(">>%s\n\n", utils.HexRepresentation(p))
					toHumanLanguage(p, true)
					log.Println("///////////////////////////////////////////////////////////////////////////////////////\n\n")
				}
				_, ew := device.Write(buf[0:nr])
				if ew != nil {
					log.Fatal(ew)
				}
			}
			if er != nil {
				if er == io.EOF {
					log.Fatal(er)
				}
			}
		}
	}()

	for {
		buf := make([]byte, 32*1024)
		nr, er := device.Read(buf)
		if nr > 0 {
			packets := parseBySTX(buf[:nr])
			for _, p := range packets {
				log.Println("////////////////////////////////////////RESPONSE///////////////////////////////////////")
				log.Printf("<<%s\n", utils.HexRepresentation(p))
				toHumanLanguage(p, false)
				log.Println("///////////////////////////////////////////////////////////////////////////////////////\n\n")
			}
			_, ew := TestDriverConn.Write(buf[0:nr])
			if ew != nil {
				log.Fatal(ew)
			}
		}
		if er != nil {
			if er == io.EOF {
				log.Fatal(er)
			}
		}
	}
}

type packetID = byte
type taskAction = code

var packetSet = make(map[packetID]taskAction)

type packet = []byte

func unstuff(data []byte) (unStuffedData []byte, isDamagedPacket bool) {
	var isESC bool
	for i := range data {
		switch {
		case isESC:
			switch data[i] {
			case codeTESC:
				unStuffedData = append(unStuffedData, codeESC)
			case codeTSTX:
				unStuffedData = append(unStuffedData, codeSTX)
			default:
				logging.Error("Нарушение обмена - пакет отброшен")
				return nil, true
			}
			isESC = false
		case data[i] == codeESC:
			isESC = true
		default:
			unStuffedData = append(unStuffedData, data[i])
		}
	}
	if isESC {
		logging.Error("Байт 'ESC' в конце пакета")
		return nil, true
	}
	return
}
func handlerPanic(){
	if err := recover(); err != nil {
		logging.Error("Получен битый пакет")
	}
}
func toHumanLanguage(machineLanguage []byte, isRequest bool) {
	defer handlerPanic()
	var humanLanguageMessage string
	if machineLanguage == nil {
		logging.Error("Получен пустой пакет")
		return
	}
	data, isDamaged := unstuff(machineLanguage)
	if isDamaged {
		return
	}
	if data[0] != codeSTX {
		logging.Error("Байт 'STX' не найден")
		return
	}
	humanLanguageMessage += fmt.Sprintf("\t[%02X] - STX\n", data[0])
	length := getFromLen(data[1:3])
	if len(data)-1-4 != length {
		logging.Error("Длина чанка 'DATA' не совпадает с чанком 'LEN'")
		return
	}
	humanLanguageMessage += fmt.Sprintf("\t%s - LEN = %d\n", utils.HexRepresentation(data[1:3]), length)
	packetID := data[3]
	task := data[4 : len(data)-1]
	crc := data[len(data)-1]
	if crc != getCRC(packetID, task) {
		logging.Error("Контрольная сумма не совпадает")
		return
	}
	humanLanguageMessage += fmt.Sprintf("\t[%02X] - PacketID = %d\n", packetID, int(packetID))
	humanLanguageMessage += fmt.Sprintf("\t%s - DATA\n", utils.HexRepresentation(task))
	if isRequest {
		taskMessage, isDamaged := parseRequestTask(task, packetID)
		if isDamaged {
			return
		}
		humanLanguageMessage += taskMessage
	} else {
		taskMessage, isDamaged := parseResponseTask(task, packetID)
		if isDamaged {
			return
		}
		humanLanguageMessage += taskMessage
	}
	humanLanguageMessage += fmt.Sprintf("\t[%02X] - CRC\n", crc)
	fmt.Println(humanLanguageMessage)
}
func byteToString(b byte) string {
	return fmt.Sprintf("[%02X]", b)
}

func parseTLV(data []byte) (humanLanguageMessage string, isDamaged bool) {
	if len(data) == 0 {
		logging.Error("Получена пустая TLV-структура")
		isDamaged = true
		return
	}

	switch data[0] {
	case 0:
		humanLanguageMessage += fmt.Sprintf("\t\t\t\t[%02X] - Flags = %s\n", data[0], "Не выводить на печать")
	case 1:
		humanLanguageMessage += fmt.Sprintf("\t\t\t\t[%02X] - Flags = %s\n", data[0], "Выводить на печать")
	default:
		logging.Error("Неизвестный флаг")
		isDamaged = true
		return
	}

	blockCount := int(data[1])
	humanLanguageMessage += fmt.Sprintf("\t\t\t\t[%02X] - BlockCount = %d\n", data[1], blockCount)

	blockNumber := int(data[2])
	humanLanguageMessage += fmt.Sprintf("\t\t\t\t[%02X] - BlockNumber = %d\n", data[2], blockNumber)

	tlv := data[3:]
	tag := tlv[:2]
	tagNum, isDamaged := fromTwoBytes(tag)
	if isDamaged {
		return
	}
	tagInfo := tagsDescription[tagNum]

	humanLanguageMessage += fmt.Sprintf("\t\t\t\t%s - Tag = %d | TagDescription = %s\n", utils.HexRepresentation(tag), tagNum, tagInfo.Description)

	length := tlv[2:4]
	l := getFromLen(length)

	humanLanguageMessage += fmt.Sprintf("\t\t\t\t%s - Length = %d\n", utils.HexRepresentation(length), l)

	val := tlv[4:]
	switch tagInfo.Type {
	case StringTagType:
		runes := make([]rune, 0)
		for _, b := range val {
			runes = append(runes, fromCP866[b])
		}
		humanLanguageMessage += fmt.Sprintf("\t\t\t\t%s - StringValue = %s\n", utils.HexRepresentation(val), string(runes))
	default:
		humanLanguageMessage += fmt.Sprintf("\t\t\t\t%s - Value\n", utils.HexRepresentation(val))
	}
	return
}
func parseResponseTask(data []byte, packetID byte) (humanLanguageMessage string, isDamaged bool) {
	if len(data) == 0 {
		logging.Error("Получен пустой ответ")
		isDamaged = true
		return
	}
	taskCommand := packetSet[packetID]
	switch taskCommand {
	case codeAdd:
		switch data[0] {
		case codePending:
			taskResult := "Задание помещено в буфер, но пока не исполняется."
			humanLanguageMessage += fmt.Sprintf("\t\t[%02X] - Pending[Add] = %s\n", data[0], taskResult)
		case codeInProgress:
			taskResult := "Задание помещено в буфер и уже исполняется."
			humanLanguageMessage += fmt.Sprintf("\t\t[%02X] - InProgress[Add] = %s\n", data[0], taskResult)
		case codeWaiting:
			taskResult := "Задание помещено в буфер и уже ожидает данных от внешнего устройства."
			humanLanguageMessage += fmt.Sprintf("\t\t[%02X] - Waiting[Add] = %s\n", data[0], taskResult)
		case codeResult:
			taskResult := "Задание уже находилось в буфере и ККТ успела его успешно выполнить."
			humanLanguageMessage += fmt.Sprintf("\t\t[%02X] - Result[Add] = %s\n", data[0], taskResult)
			humanLanguageMessage += fmt.Sprintf("\t\t%s - TaskResultValue\n", utils.HexRepresentation(data[1:]))
		case codeError:
			taskResult := "Задание уже находилось в буфере, и ККТ успела его выполнить, но возникла ошибка"
			humanLanguageMessage += fmt.Sprintf("\t\t[%02X] - Error[Add] = %s\n", data[0], taskResult)
			humanLanguageMessage += fmt.Sprintf("\t\t%s - TaskResultValue\n", utils.HexRepresentation(data[1:]))
		case codeStopped:
			taskResult := `Задание добавлено в буфер, но буфер находится в состоянии "Error" из-за ошибки, возникшей при исполнении ККТ задания TIdErr.`
			humanLanguageMessage += fmt.Sprintf("\t\t[%02X] - Stopped[Add] = %s\n", data[0], taskResult)
			humanLanguageMessage += fmt.Sprintf("\t\t[%02X] - TaskIDErr\n", data[1])
		case codeE_IllegalValue:
			taskResult := `Задание не может быть помещено в буфер – недопустимое значение параметра. PIdx = 0 (для Flags), PIdx = 1 (для TId).`
			humanLanguageMessage += fmt.Sprintf("\t\t[%02X] - E_IllegalValue[Add] = %s\n", data[0], taskResult)
			Pidx := data[1]
			if Pidx == 0 {
				taskResult = "Недопустимое значение для параметра Flags"
			} else {
				taskResult = "Недопустимое значение для параметра Tid"
			}
			humanLanguageMessage += fmt.Sprintf("\t\t[%02X] - Pidx - %s\n", data[1], taskResult)
		case codeE_AlreadyExists:
			taskResult := `Задание не может быть помещено в буфер – в буфере уже есть задание с таким TId (и это не может быть повтором).`
			humanLanguageMessage += fmt.Sprintf("\t\t[%02X] - E_AlreadyExists[Add] = %s\n", data[0], taskResult)
		case codeE_Overflow:
			taskResult := `Задание не может быть помещено в буфер – не достаточно места в буфере, так как не удается удалить задание с TId2 (можно затереть только удачно завершенное задание с не установленным NeedResult).`
			humanLanguageMessage += fmt.Sprintf("\t\t[%02X] - E_Overflow[Add] = %s\n", data[0], taskResult)
			humanLanguageMessage += fmt.Sprintf("\t\t[%02X] - overflowTaskID\n", data[1])
		default:
			logging.Error("Неизвестная команда ответа")
			isDamaged = true
			return
		}
	case codeAck:
		switch data[0] {
		case codePending:
			taskResult := "Задание еще не начали исполнять – команда не выполнена. "
			humanLanguageMessage += fmt.Sprintf("\t\t[%02X] - Pending[Ack] = %s\n", data[0], taskResult)
		case codeInProgress:
			taskResult := "Задание в данный момент исполняется – команда не выполнена."
			humanLanguageMessage += fmt.Sprintf("\t\t[%02X] - InProgress[Ack] = %s\n", data[0], taskResult)
		case codeWaiting:
			taskResult := "ККТ сняла флаг для задания и прекращает ждать данные от внешнего устройства. Состояние задание меняется на Result или Error на усмотрение прошивки. "
			humanLanguageMessage += fmt.Sprintf("\t\t[%02X] - Waiting[Ack] = %s\n", data[0], taskResult)
			delete(packetSet, packetID)
		case codeResult:
			taskResult := "ККТ сняла флаг для успешно исполненного задания. "
			humanLanguageMessage += fmt.Sprintf("\t\t[%02X] - Result[Ack] = %s\n", data[0], taskResult)
			delete(packetSet, packetID)
		case codeError:
			taskResult := "ККТ сняла флаг для исполненного с ошибкой задания. "
			humanLanguageMessage += fmt.Sprintf("\t\t[%02X] - Error[Ack] = %s\n", data[0], taskResult)
			delete(packetSet, packetID)
		case codeStopped:
			taskResult := "Команда не выполнена, так как задание отменено из-за ошибки, возникшей при исполнении другого задания."
			humanLanguageMessage += fmt.Sprintf("\t\t[%02X] - Stopped[Ack] = %s\n", data[0], taskResult)
		case codeE_IllegalValue:
			taskResult := "Недопустимое значение TId (PIdx = 0)."
			humanLanguageMessage += fmt.Sprintf("\t\t[%02X] - E_IllegalValue[Ack] = %s\n", data[0], taskResult)
		case codeE_NotFound:
			taskResult := "Задания с таким TId нет в буфере."
			humanLanguageMessage += fmt.Sprintf("\t\t[%02X] - E_NotFound[Ack] = %s\n", data[0], taskResult)
		default:
			logging.Error("Неизвестная команда ответа")
			isDamaged = true
			return
		}
	case codeReq:
		switch data[0] {
		case codePending:
			taskResult := "Задание еще не начали исполнять."
			humanLanguageMessage += fmt.Sprintf("\t\t[%02X] - Pending[Req] = %s\n", data[0], taskResult)
		case codeInProgress:
			taskResult := "Задание в данный момент исполняется. "
			humanLanguageMessage += fmt.Sprintf("\t\t[%02X] - InProgress[Req] = %s\n", data[0], taskResult)
		case codeWaiting:
			taskResult := "Задание ожидает данных от внешнего устройства. "
			humanLanguageMessage += fmt.Sprintf("\t\t[%02X] - Waiting[Req] = %s\n", data[0], taskResult)
		case codeResult:
			taskResult := "Задание исполнено успешно."
			humanLanguageMessage += fmt.Sprintf("\t\t[%02X] - Result[Req] = %s\n", data[0], taskResult)
			humanLanguageMessage += fmt.Sprintf("\t\t%s - TaskResultValue\n", utils.HexRepresentation(data[1:]))
		case codeError:
			taskResult := "Задание исполнено с ошибкой."
			humanLanguageMessage += fmt.Sprintf("\t\t[%02X] - Error[Req] = %s\n", data[0], taskResult)
			humanLanguageMessage += fmt.Sprintf("\t\t%s - TaskResultValue\n", utils.HexRepresentation(data[1:]))
		case codeStopped:
			taskResult := "Задание отменено из-за ошибки, возникшей при исполнении задания"
			humanLanguageMessage += fmt.Sprintf("\t\t[%02X] - Stopped[Req] = %s\n", data[0], taskResult)
		case codeE_IllegalValue:
			taskResult := "Недопустимое значение TId (PIdx = 0). "
			humanLanguageMessage += fmt.Sprintf("\t\t[%02X] - E_IllegalValue[Req] = %s\n", data[0], taskResult)
		case codeE_NotFound:
			taskResult := "Задания с таким TId нет в буфере. "
			humanLanguageMessage += fmt.Sprintf("\t\t[%02X] - E_NotFound[Req] = %s\n", data[0], taskResult)
		default:
			logging.Error("Неизвестная команда ответа")
			isDamaged = true
			return
		}
	case codeAbort:
		switch data[0] {
		case codeResult:
			taskResult := "Все задания удалены и прерваны. "
			humanLanguageMessage += fmt.Sprintf("\t\t[%02X] - Result[Abort] = %s\n", data[0], taskResult)
		case codeInProgress:
			taskResult := `Все задания удалены, кроме того TId, которое исполняется и не может быть прервано. `
			humanLanguageMessage += fmt.Sprintf("\t\t[%02X] - InProgress[Abort] = %s\n", data[0], taskResult)
			humanLanguageMessage += fmt.Sprintf("\t\t[%02X] - TaskId\n", data[1])
		default:
			logging.Error("Неизвестная команда ответа")
			isDamaged = true
			return
		}
	default:
		logging.Error("Неизвестная команда ответа")
		isDamaged = true
		return
	}
	return
}
func parseRequestTask(data []byte, packetID byte) (humanLanguageMessage string, isDamaged bool) {
	var taskAction string
	if len(data) == 0 {
		logging.Error("Получено пустое задание")
		isDamaged = true
		return
	}
	switch data[0] {
	case codeAdd:
		taskAction = "Add Добавление задания в буфер"

		humanLanguageMessage += fmt.Sprintf("\t\t[%02X] - TaskAction = %s\n", data[0], taskAction)
		flags := parseFlags(data[1])
		humanLanguageMessage += fmt.Sprintf("\t\t[%02X] - TaskFlags = %s\n", data[1], flags)
		taskID := int(data[2])
		humanLanguageMessage += fmt.Sprintf("\t\t[%02X] - TaskID = %d\n", data[2], taskID)
		accessPassword := data[3:5]
		humanLanguageMessage += fmt.Sprintf("\t\t%s - AccessPassword \n", utils.HexRepresentation(accessPassword))
		value := data[5:]
		humanLanguageMessage += fmt.Sprintf("\t\t%s - Value\n", utils.HexRepresentation(value))

		if len(value) == 0 {
			logging.Error("Получена пустая команда")
			isDamaged = true
			return
		}

		cmdDescription := cmdSet[value[0]]
		humanLanguageMessage += fmt.Sprintf("\t\t\t[%02X] - Command description = %s", value[0], cmdDescription)
		if value[0] == 0xA4 {
			humanLanguageMessage += ". " + cmdFNSet[value[1]]
		}
		humanLanguageMessage += "\n"
		humanLanguageMessage += fmt.Sprintf("\t\t\t%s - Command value\n", utils.HexRepresentation(value[1:]))
		if value[0] == 0xE8 {
			var parsedTlv string
			parsedTlv, isDamaged = parseTLV(value[1:])
			if isDamaged {
				return
			}
			humanLanguageMessage += parsedTlv
		}
		if value[0] == 0x91 {
			register := value[1]
			humanLanguageMessage += fmt.Sprintf("\t\t\t\t[%02X] - Register description= %s\n", register, registersDescription[register])

			param1 := value[2]
			humanLanguageMessage += fmt.Sprintf("\t\t\t\t[%02X] - Param 1\n", param1)
			param2 := value[3]
			humanLanguageMessage += fmt.Sprintf("\t\t\t\t[%02X] - Param 2\n", param2)
		}
	case codeAck:
		taskAction = "Ack Подтверждение получения результата"
		humanLanguageMessage += fmt.Sprintf("\t\t[%02X] - TaskAction = %s\n", data[0], taskAction)
		taskID := int(data[1])
		humanLanguageMessage += fmt.Sprintf("\t\t[%02X] - TaskID = %d\n", data[1], taskID)
	case codeReq:
		taskAction = "Req Получение состояния задания"
		humanLanguageMessage += fmt.Sprintf("\t\t[%02X] - TaskAction = %s\n", data[0], taskAction)
		taskID := int(data[1])
		humanLanguageMessage += fmt.Sprintf("\t\t[%02X] - TaskID = %d\n", data[1], taskID)
	case codeAbort:
		taskAction = "Abort Очищение буфера"
		humanLanguageMessage += fmt.Sprintf("\t\t[%02X] - TaskAction = %s\n", data[0], taskAction)
	case codeAckAdd:
		taskAction = "AckAdd Добавление задания в буфер с одновременным подтверждением"
		humanLanguageMessage += fmt.Sprintf("\t\t[%02X] - TaskAction = %s\n", data[0], taskAction)

		ackTaskID := int(data[1])
		humanLanguageMessage += fmt.Sprintf("\t\t[%02X] - AckTaskID = %d\n", data[1], ackTaskID)

		flags := parseFlags(data[2])
		humanLanguageMessage += fmt.Sprintf("\t\t[%02X] - TaskFlags = %s\n", data[2], flags)
		addTaskID := int(data[3])
		humanLanguageMessage += fmt.Sprintf("\t\t[%02X] - AddTaskID = %d\n", data[3], addTaskID)
		accessPassword := data[4:6]
		humanLanguageMessage += fmt.Sprintf("\t\t%s - AccessPassword \n", utils.HexRepresentation(accessPassword))
		value := data[6:]
		humanLanguageMessage += fmt.Sprintf("\t\t%s - Value\n", utils.HexRepresentation(value))

		if len(value) == 0 {
			logging.Error("Получена пустая команда")
			isDamaged = true
			return
		}

		cmdDescription := cmdSet[value[0]]
		humanLanguageMessage += fmt.Sprintf("\t\t\t[%02X] - Command description = %s", value[0], cmdDescription)
		if value[0] == 0xA4 {
			humanLanguageMessage += ". " + cmdFNSet[value[1]]
		}
		humanLanguageMessage += "\n"
		humanLanguageMessage += fmt.Sprintf("\t\t\t%s - Command value\n", utils.HexRepresentation(value[1:]))
		if value[0] == 0xE8 {
			var parsedTlv string
			parsedTlv, isDamaged = parseTLV(value[1:])
			if isDamaged {
				return
			}
			humanLanguageMessage += parsedTlv
		}

	default:
		logging.Error("Неизвестная команда задания")
		isDamaged = true
		return
	}
	packetSet[packetID] = data[0]
	return
}

const (
	flagNeedResult byte = 1 << iota
	flagIgnoreError
	flagWaitAsyncData
)

func parseFlags(flags byte) string {
	result := make([]string, 0)
	if flags&flagNeedResult == 1 {
		result = append(result, "NeedResult ")
	}
	if flags&flagIgnoreError == 1 {
		result = append(result, "IgnoreError ")
	}
	if flags&flagWaitAsyncData == 1 {
		result = append(result, "WaitAsyncData ")
	}
	return strings.Join(result, ";")
}

func getFromLen(length []byte) int {
	return int(length[1])<<7 + int(length[0])
}

func fromTwoBytes(data []byte) (i uint16, isDamaged bool) {
	if len(data) != 2 {
		logging.Error("Неверный размер данных")
		isDamaged = true
		return
	}
	i = uint16(data[1])<<8 + uint16(data[0])
	return
}

func parseBySTX(data []byte) (packets []packet) {
	for _, b := range data {
		if b == codeSTX || len(packets) == 0 {
			packets = append(packets, make([]byte, 0))
		}
		packets[len(packets)-1] = append(packets[len(packets)-1], b)
	}
	return
}

const (
	seed byte = 0xff
	poly byte = 0x31
)

func getCRC(id byte, data []byte) (CRC byte) {
	CRC = seed
	for _, el := range append([]byte{id}, data...) {
		CRC = CRC ^ el
		for j := 0; j < 8; j++ {
			if CRC&0x80 != 0 {
				CRC = (CRC << 1) ^ poly
			} else {
				CRC = CRC << 1
			}
		}
	}
	return CRC & 0xFF
}

type code = byte

const (
	// Транспортный уровень
	codeSTX  code = 0xFE
	codeESC  code = 0xFD
	codeTSTX code = 0xEE
	codeTESC code = 0xED

	// Статус задания
	codePending     code = 0xA1
	codeInProgress  code = 0xA2
	codeResult      code = 0xA3
	codeError       code = 0xA4
	codeStopped     code = 0xA5
	codeAsyncResult code = 0xA6
	codeAsyncError  code = 0xA7
	codeWaiting     code = 0xA8

	// Ошибки
	codeE_Overflow      code = 0xB1
	codeE_AlreadyExists code = 0xB2
	codeE_NotFound      code = 0xB3
	codeE_IllegalValue  code = 0xB4

	// Команды буфера
	codeAdd    code = 0xC1
	codeAck    code = 0xC2
	codeReq    code = 0xC3
	codeAbort  code = 0xC4
	codeAckAdd code = 0xC5
)

var cmdSet = map[byte]string{
	0x3F: "Запрос состояния ККТ",
	0x43: "Изменение итога чека",
	0x45: "Запрос кода состояния ККТ",
	0x46: "Чтение таблицы",
	0x47: "Гудок",
	0x48: "Выход из текущего режима",
	0x49: "Внесение денег",
	0x4A: "Закрыть чек (со сдачей)",
	0x4B: "Программирование времени",
	0x4C: "Печать строки",
	0x4D: "Запрос наличных",
	0x4F: "Выплата денег",
	0x50: "Программирование таблицы",
	0x52: "Регистрация",
	0x56: "Вход в режим",
	0x57: "Возврат",
	0x58: "Получение последнего сменного итога",
	0x59: "Аннулирование всего чека",
	0x5A: "Снятие суточного отчета с гашением (закрытие смены)",
	0x61: "Ввод заводского номера",
	0x64: "Программирование даты",
	0x67: "Начало снятия отчета без гашения",
	0x6B: "Технологическое обнуление ККТ",
	0x6C: "Печать клише чека",
	0x6D: "Ввод кода защиты ККТ",
	0x71: "Инициализация таблиц нач. значениями",
	0x73: "Печать нижней части чека",
	0x74: "Запрос активизированности кода защиты ККТ",
	0x75: "Отрезать чек",
	0x77: "Общее гашение",
	0x78: "Отмена последней скидки/надбавки на регистрацию",
	0x79: "Начало считывания штрихкода",
	0x7A: "Получить очередной блок данных",
	0x7B: "Очистить массив штрихкодов",
	0x7C: "Печать штрихкода по номеру",
	0x7D: "Состояние массива штрихкодов и картинок",
	0x7E: "Добавить строку картинки в буфер в ОЗУ",
	0x7F: "Распечатать картинку из буфера в ОЗУ",
	0x80: "Открыть денежный ящик",
	0x82: "Демонстрационная печать",
	0x84: "Получение очередного блока данных ПО ККТ",
	0x85: "Импульсное открытие денежного ящика",
	0x86: "Получить очередную строку картинки по номеру",
	0x87: "Печать поля",
	0x88: "Звуковой сигнал",
	0x8A: "Очистить массив картинок",
	0x8B: "Добавить строку картинки",
	0x8C: "Статус массива картинок",
	0x8D: "Печать картинки по номеру",
	0x8E: "Печать картинки с ПК",
	0x8F: "Передать данные в порт",
	0x90: "Параметры картинки в массиве",
	0x91: "Считать регистр",
	0x92: "Открыть чек",
	0x95: "Повторная печать последнего документа",
	0x99: "Расчет по чеку",
	0x9A: "Открыть смену",
	0x9B: "Сторно расчета по чеку",
	0x9C: "Начало считывания дампа",
	0x9D: "Получение версии",
	0x9E: "Закрыть картинку",
	0x9F: "Начать считывание картинки по номеру",
	0xA4: "Команды обмена информационными данными с ФН",
	0xA5: "Прочитать данные для ККТ",
	0xA6: "Активизация ФН",
	0xA7: "Закрытие архива ФН",
	0xA8: "Печать итогов регистрации/перерегистрации ККТ",
	0xAB: "Печать документа по номеру",
	0xB3: "Получить последний код ошибки",
	0xB8: "Регистрация налога на весь чек",
	0xBA: "Регистрация скидки/надбавки",
	0xBE: "Запрос состояния ЭЖ",
	0xBF: "Формирование реквизита",
	0xC1: "Печать штрихкода",
	0xC2: "Печать штрихкода (добавление данных)",
	0xC3: "Получение данных ЭЖ",
	0xCE: "Выключение",
	0xE6: "Регистрация позиции",
	0xE8: "Запись реквизита",
	0xE9: "Чтение реквизита",
	0xEA: "Комплексная команда регистрации позиции: начать формирование позиции",
	0xEB: "Комплексная команда формирования позиции: завершить формирование позиции",
}

var cmdFNSet = map[byte]string{
	0x10: "Запрос параметров текущей смены",
	0x20: "Получить статус информационного обмена",
	0x30: "Запрос статуса ФН",
	0x31: "Запрос номера ФН",
	0x32: "Запрос срока действия ФН",
	0x33: "Запрос версии ФН",
	0x35: "Запрос последних ошибок ФН",
	0x40: "Найти фискальный документ по номеру",
	0x41: "Запрос квитанции о получение фискального документа ОФД по номеру документа",
	0x42: "Запрос количества ФД, на которые нет квитанции",
	0x43: "Запрос итогов фискализации ФН",
	0x44: "Запрос параметра фискализации ФН",
	0x45: "Запрос фискального документа в TLV формате",
}

type TagType string

const (
	NumTagType    TagType = "Целое"
	StringTagType TagType = "Строка"
	ASCIITagType  TagType = "ASCII"
	StructTagType TagType = "Структура"
	FlagTagType   TagType = "Флаг"
)

type TagModel struct {
	Description string
	Type        TagType
}

var tagsDescription = map[uint16]TagModel{
	1000: {Description: "наименование документа", Type: StringTagType},
	1001: {Description: "признак автоматического режима", Type: StringTagType},
	1002: {Description: "признак автономного режима", Type: StringTagType},
	1008: {Description: "телефон или электронный адрес покупателя", Type: StringTagType},
	1009: {Description: "адрес расчетов", Type: StringTagType},
	1012: {Description: "дата, время", Type: NumTagType},
	1013: {Description: "заводской номер ККТ", Type: StringTagType},
	1017: {Description: "ИНН ОФД", Type: StringTagType},
	1018: {Description: "ИНН пользователя", Type: StringTagType},
	1020: {Description: "сумма расчета, указанного в чеке (БСО)", Type: StringTagType},
	1021: {Description: "кассир", Type: StringTagType},
	1022: {Description: "код ответа ОФД", Type: StringTagType},
	1031: {Description: "сумма по чеку (БСО) наличными", Type: StringTagType},
	1036: {Description: "номер автомата", Type: StringTagType},
	1037: {Description: "регистрационный номер ККТ", Type: StringTagType},
	1038: {Description: "номер смены", Type: StringTagType},
	1040: {Description: "номер ФД", Type: StringTagType},
	1041: {Description: "номер ФН", Type: StringTagType},
	1042: {Description: "номер чека за смену", Type: StringTagType},
	1046: {Description: "наименование ОФД", Type: StringTagType},
	1048: {Description: "наименование пользователя", Type: StringTagType},
	1050: {Description: "признак исчерпания ресурса ФН", Type: StringTagType},
	1051: {Description: "признак окончания ресурса ФН ", Type: StringTagType},
	1052: {Description: "признак заполнения памяти ФН", Type: StringTagType},
	1053: {Description: "признак превышения времени ожидания ответа ОФД", Type: StringTagType},
	1054: {Description: "признак расчета", Type: StringTagType},
	1055: {Description: "применяемая система налогообложения", Type: NumTagType},
	1056: {Description: "признак шифрования", Type: StringTagType},
	1057: {Description: "признак агента ", Type: NumTagType},
	1060: {Description: "адрес сайта ФНС", Type: StringTagType},
	1062: {Description: "системы налогообложения", Type: StringTagType},
	1068: {Description: "сообщение оператора для ФН", Type: StringTagType},
	1077: {Description: "ФПД", Type: StringTagType},
	1078: {Description: "ФПО", Type: StringTagType},
	1081: {Description: "сумма по чеку (БСО) электронными", Type: StringTagType},
	1097: {Description: "количество непереданных ФД", Type: StringTagType},
	1098: {Description: "дата и время первого из непереданных ФД", Type: StringTagType},
	1101: {Description: "код причины перерегистрации", Type: StringTagType},
	1102: {Description: "сумма НДС чека по ставке 18%", Type: StringTagType},
	1103: {Description: "сумма НДС чека по ставке 10%", Type: StringTagType},
	1104: {Description: "сумма расчета по чеку с НДС по ставке 0%", Type: StringTagType},
	1105: {Description: "сумма расчета по чеку без НДС", Type: StringTagType},
	1106: {Description: "сумма НДС чека по расч. ставке 18/118", Type: StringTagType},
	1107: {Description: "сумма НДС чека по расч. ставке 10/110", Type: StringTagType},
	1108: {Description: "признак ККТ для расчетов только в Интернет", Type: FlagTagType},
	1109: {Description: "признак расчетов за услуги", Type: StringTagType},
	1110: {Description: "признак АС БСО", Type: StringTagType},
	1111: {Description: "общее количество ФД за смену", Type: StringTagType},
	1116: {Description: "номер первого непереданного документа", Type: StringTagType},
	1117: {Description: "адрес электронной почты отправителя чека", Type: StringTagType},
	1118: {Description: "количество кассовых чеков (БСО) за смену", Type: StringTagType},
	1126: {Description: "признак проведения лотереи", Type: StringTagType},
	1129: {Description: "счетчики операций приход", Type: StringTagType},
	1130: {Description: "счетчики операций возврат прихода", Type: StringTagType},
	1131: {Description: "счетчики операций расход", Type: StringTagType},
	1132: {Description: "счетчики операций возврат расхода", Type: StringTagType},
	1133: {Description: "счетчики операций по чекам коррекции", Type: StringTagType},
	1134: {Description: "количество чеков (БСО) со всеми признаками расчетов", Type: StringTagType},
	1135: {Description: "количество чеков по признаку расчетов", Type: StringTagType},
	1136: {Description: "итоговая сумма в чеках (БСО) наличными денежными средствами", Type: StringTagType},
	1138: {Description: "итоговая сумма в чеках (БСО) электронными средствами платежа", Type: StringTagType},
	1139: {Description: "сумма НДС по ставке 18%", Type: StringTagType},
	1140: {Description: "сумма НДС по ставке 10%", Type: StringTagType},
	1141: {Description: "сумма НДС по расч. ставке 18/118", Type: StringTagType},
	1142: {Description: "сумма НДС по расч. ставке 10/110", Type: StringTagType},
	1143: {Description: "сумма расчетов с НДС по ставке 0%", Type: StringTagType},
	1144: {Description: "количество чеков коррекции", Type: StringTagType},
	1145: {Description: "счетчики коррекций приход", Type: StringTagType},
	1146: {Description: "счетчики коррекций расход", Type: StringTagType},
	1148: {Description: "количество самостоятельных корректировок", Type: StringTagType},
	1149: {Description: "количество корректировок по предписанию", Type: StringTagType},
	1151: {Description: "сумма коррекций НДС по ставке 18%", Type: StringTagType},
	1152: {Description: "сумма коррекций НДС по ставке 10%", Type: StringTagType},
	1153: {Description: "сумма коррекций НДС по расч. ставке 18/118", Type: StringTagType},
	1154: {Description: "сумма коррекций НДС по расч. ставке 10/110", Type: StringTagType},
	1155: {Description: "сумма коррекций с НДС по ставке 0%", Type: StringTagType},
	1157: {Description: "счетчики итогов ФН", Type: StringTagType},
	1158: {Description: "счетчики итогов непереданных ФД", Type: StringTagType},
	1173: {Description: "тип коррекции", Type: StringTagType},
	1183: {Description: "сумма расчетов без НДС", Type: StringTagType},
	1184: {Description: "сумма коррекций без НДС", Type: StringTagType},
	1187: {Description: "место расчетов", Type: StringTagType},
	1188: {Description: "версия ККТ", Type: StringTagType},
	1189: {Description: "версия ФФД ККТ", Type: StringTagType},
	1190: {Description: "версия ФФД ФН", Type: StringTagType},
	1192: {Description: "дополнительный реквизит чека (БСО)", Type: StringTagType},
	1193: {Description: "признак проведения азартных игр", Type: StringTagType},
	1194: {Description: "счетчики итогов смены", Type: StringTagType},
	1196: {Description: "QR-код", Type: StringTagType},
	1201: {Description: "общая итоговая сумма в чеках (БСО)", Type: StringTagType},
	1203: {Description: "ИНН кассира", Type: StringTagType},
	1205: {Description: "коды причин изменения сведений о ККТ", Type: StringTagType},
	1206: {Description: "сообщение оператора", Type: StringTagType},
	1207: {Description: "признак торговли подакцизными товарами", Type: StringTagType},
	1208: {Description: "сайт для получения чека", Type: StringTagType},
	1209: {Description: "версия ФФД", Type: StringTagType},
	1213: {Description: "ресурс ключей ФП", Type: StringTagType},
	1215: {Description: "сумма по чеку (БСО) предоплатой (зачетом аванса)", Type: StringTagType},
	1216: {Description: "сумма по чеку (БСО) постоплатой (в кредит)", Type: StringTagType},
	1217: {Description: "сумма по чеку (БСО) встречным предоставлением", Type: StringTagType},
	1218: {Description: "итоговая сумма в чеках (БСО) предоплатами (авансами)", Type: StringTagType},
	1219: {Description: "итоговая сумма в чеках (БСО) постоплатами (кредитами)", Type: StringTagType},
	1220: {Description: "итоговая сумма в чеках (БСО) встречными предоставлениями", Type: StringTagType},
	1221: {Description: "признак установки принтера в автомате", Type: StringTagType},
	1059: {Description: "предмет расчета", Type: StringTagType},
	1214: {Description: "признак способа расчета", Type: StringTagType},
	1212: {Description: "признак предмета расчета", Type: StringTagType},
	1222: {Description: "признак агента по предмету расчета", Type: StringTagType},
	1223: {Description: "данные агента", Type: StringTagType},
	1075: {Description: "телефон оператора перевода", Type: StringTagType},
	1044: {Description: "операция платежного агента", Type: StringTagType},
	1073: {Description: "телефон платежного агента", Type: StringTagType},
	1074: {Description: "телефон оператора по приему платежей", Type: StringTagType},
	1026: {Description: "наименование оператора перевода", Type: StringTagType},
	1005: {Description: "адрес оператора перевода", Type: StringTagType},
	1016: {Description: "ИНН оператора перевода ", Type: StringTagType},
	1224: {Description: "данные поставщика", Type: StringTagType},
	1171: {Description: "телефон поставщика", Type: NumTagType},
	1225: {Description: "наименование поставщика", Type: StringTagType},
	1226: {Description: "ИНН поставщика", Type: StringTagType},
	1030: {Description: "наименование предмета расчета", Type: StringTagType},
	1197: {Description: "единица измерения предмета расчета", Type: StringTagType},
	1162: {Description: "код товарной номенклатуры", Type: StringTagType},
	1079: {Description: "цена за единицу предмета расчета с учетом скидок и наценок", Type: StringTagType},
	1198: {Description: "размер НДС за единицу предмета расчета", Type: StringTagType},
	1023: {Description: "количество предмета расчета", Type: StringTagType},
	1199: {Description: "ставка НДС ", Type: StringTagType},
	1200: {Description: "сумма НДС за предмет расчета", Type: StringTagType},
	1043: {Description: "стоимость предмета расчета с учетом скидок и наценок", Type: StringTagType},
	1191: {Description: "дополнительный реквизит предмета расчета", Type: StringTagType},
	1084: {Description: "дополнительный реквизит пользователя", Type: StructTagType},
	1085: {Description: "наименование дополнительного реквизита пользователя", Type: StringTagType},
	1086: {Description: "значение дополнительного реквизита пользователя", Type: StringTagType},
	1174: {Description: "основание для коррекции", Type: StringTagType},
	1177: {Description: "наименование основания для коррекции", Type: StringTagType},
	1178: {Description: "дата документа основания для коррекции", Type: StringTagType},
	1179: {Description: "номер документа основания для коррекции", Type: StringTagType},
}

var fromCP866 = map[byte]rune{
	0x00: 0x0000, // NULL

	0x01: 0x0001, // START OF HEADING

	0x02: 0x0002, // START OF TEXT

	0x03: 0x0003, // END OF TEXT

	0x04: 0x0004, // END OF TRANSMISSION

	0x05: 0x0005, // ENQUIRY

	0x06: 0x0006, // ACKNOWLEDGE

	0x07: 0x0007, // BELL

	0x08: 0x0008, // BACKSPACE

	0x09: 0x0009, // HORIZONTAL TABULATION

	0x0a: 0x000a, // LINE FEED

	0x0b: 0x000b, // VERTICAL TABULATION

	0x0c: 0x000c, // FORM FEED

	0x0d: 0x000d, // CARRIAGE RETURN

	0x0e: 0x000e, // SHIFT OUT

	0x0f: 0x000f, // SHIFT IN

	0x10: 0x0010, // DATA LINK ESCAPE

	0x11: 0x0011, // DEVICE CONTROL ONE

	0x12: 0x0012, // DEVICE CONTROL TWO

	0x13: 0x0013, // DEVICE CONTROL THREE

	0x14: 0x0014, // DEVICE CONTROL FOUR

	0x15: 0x0015, // NEGATIVE ACKNOWLEDGE

	0x16: 0x0016, // SYNCHRONOUS IDLE

	0x17: 0x0017, // END OF TRANSMISSION BLOCK

	0x18: 0x0018, // CANCEL

	0x19: 0x0019, // END OF MEDIUM

	0x1a: 0x001a, // SUBSTITUTE

	0x1b: 0x001b, // ESCAPE

	0x1c: 0x001c, // FILE SEPARATOR

	0x1d: 0x001d, // GROUP SEPARATOR

	0x1e: 0x001e, // RECORD SEPARATOR

	0x1f: 0x001f, // UNIT SEPARATOR

	0x20: 0x0020, // SPACE

	0x21: 0x0021, // EXCLAMATION MARK

	0x22: 0x0022, // QUOTATION MARK

	0x23: 0x0023, // NUMBER SIGN

	0x24: 0x0024, // DOLLAR SIGN

	0x25: 0x0025, // PERCENT SIGN

	0x26: 0x0026, // AMPERSAND

	0x27: 0x0027, // APOSTROPHE

	0x28: 0x0028, // LEFT PARENTHESIS

	0x29: 0x0029, // RIGHT PARENTHESIS

	0x2a: 0x002a, // ASTERISK

	0x2b: 0x002b, // PLUS SIGN

	0x2c: 0x002c, // COMMA

	0x2d: 0x002d, // HYPHEN-MINUS

	0x2e: 0x002e, // FULL STOP

	0x2f: 0x002f, // SOLIDUS

	0x30: 0x0030, // DIGIT ZERO

	0x31: 0x0031, // DIGIT ONE

	0x32: 0x0032, // DIGIT TWO

	0x33: 0x0033, // DIGIT THREE

	0x34: 0x0034, // DIGIT FOUR

	0x35: 0x0035, // DIGIT FIVE

	0x36: 0x0036, // DIGIT SIX

	0x37: 0x0037, // DIGIT SEVEN

	0x38: 0x0038, // DIGIT EIGHT

	0x39: 0x0039, // DIGIT NINE

	0x3a: 0x003a, // COLON

	0x3b: 0x003b, // SEMICOLON

	0x3c: 0x003c, // LESS-THAN SIGN

	0x3d: 0x003d, // EQUALS SIGN

	0x3e: 0x003e, // GREATER-THAN SIGN

	0x3f: 0x003f, // QUESTION MARK

	0x40: 0x0040, // COMMERCIAL AT

	0x41: 0x0041, // LATIN CAPITAL LETTER A

	0x42: 0x0042, // LATIN CAPITAL LETTER B

	0x43: 0x0043, // LATIN CAPITAL LETTER C

	0x44: 0x0044, // LATIN CAPITAL LETTER D

	0x45: 0x0045, // LATIN CAPITAL LETTER E

	0x46: 0x0046, // LATIN CAPITAL LETTER F

	0x47: 0x0047, // LATIN CAPITAL LETTER G

	0x48: 0x0048, // LATIN CAPITAL LETTER H

	0x49: 0x0049, // LATIN CAPITAL LETTER I

	0x4a: 0x004a, // LATIN CAPITAL LETTER J

	0x4b: 0x004b, // LATIN CAPITAL LETTER K

	0x4c: 0x004c, // LATIN CAPITAL LETTER L

	0x4d: 0x004d, // LATIN CAPITAL LETTER M

	0x4e: 0x004e, // LATIN CAPITAL LETTER N

	0x4f: 0x004f, // LATIN CAPITAL LETTER O

	0x50: 0x0050, // LATIN CAPITAL LETTER P

	0x51: 0x0051, // LATIN CAPITAL LETTER Q

	0x52: 0x0052, // LATIN CAPITAL LETTER R

	0x53: 0x0053, // LATIN CAPITAL LETTER S

	0x54: 0x0054, // LATIN CAPITAL LETTER T

	0x55: 0x0055, // LATIN CAPITAL LETTER U

	0x56: 0x0056, // LATIN CAPITAL LETTER V

	0x57: 0x0057, // LATIN CAPITAL LETTER W

	0x58: 0x0058, // LATIN CAPITAL LETTER X

	0x59: 0x0059, // LATIN CAPITAL LETTER Y

	0x5a: 0x005a, // LATIN CAPITAL LETTER Z

	0x5b: 0x005b, // LEFT SQUARE BRACKET

	0x5c: 0x005c, // REVERSE SOLIDUS

	0x5d: 0x005d, // RIGHT SQUARE BRACKET

	0x5e: 0x005e, // CIRCUMFLEX ACCENT

	0x5f: 0x005f, // LOW LINE

	0x60: 0x0060, // GRAVE ACCENT

	0x61: 0x0061, // LATIN SMALL LETTER A

	0x62: 0x0062, // LATIN SMALL LETTER B

	0x63: 0x0063, // LATIN SMALL LETTER C

	0x64: 0x0064, // LATIN SMALL LETTER D

	0x65: 0x0065, // LATIN SMALL LETTER E

	0x66: 0x0066, // LATIN SMALL LETTER F

	0x67: 0x0067, // LATIN SMALL LETTER G

	0x68: 0x0068, // LATIN SMALL LETTER H

	0x69: 0x0069, // LATIN SMALL LETTER I

	0x6a: 0x006a, // LATIN SMALL LETTER J

	0x6b: 0x006b, // LATIN SMALL LETTER K

	0x6c: 0x006c, // LATIN SMALL LETTER L

	0x6d: 0x006d, // LATIN SMALL LETTER M

	0x6e: 0x006e, // LATIN SMALL LETTER N

	0x6f: 0x006f, // LATIN SMALL LETTER O

	0x70: 0x0070, // LATIN SMALL LETTER P

	0x71: 0x0071, // LATIN SMALL LETTER Q

	0x72: 0x0072, // LATIN SMALL LETTER R

	0x73: 0x0073, // LATIN SMALL LETTER S

	0x74: 0x0074, // LATIN SMALL LETTER T

	0x75: 0x0075, // LATIN SMALL LETTER U

	0x76: 0x0076, // LATIN SMALL LETTER V

	0x77: 0x0077, // LATIN SMALL LETTER W

	0x78: 0x0078, // LATIN SMALL LETTER X

	0x79: 0x0079, // LATIN SMALL LETTER Y

	0x7a: 0x007a, // LATIN SMALL LETTER Z

	0x7b: 0x007b, // LEFT CURLY BRACKET

	0x7c: 0x007c, // VERTICAL LINE

	0x7d: 0x007d, // RIGHT CURLY BRACKET

	0x7e: 0x007e, // TILDE

	0x7f: 0x007f, // DELETE

	0x80: 0x0410, // CYRILLIC CAPITAL LETTER A

	0x81: 0x0411, // CYRILLIC CAPITAL LETTER BE

	0x82: 0x0412, // CYRILLIC CAPITAL LETTER VE

	0x83: 0x0413, // CYRILLIC CAPITAL LETTER GHE

	0x84: 0x0414, // CYRILLIC CAPITAL LETTER DE

	0x85: 0x0415, // CYRILLIC CAPITAL LETTER IE

	0x86: 0x0416, // CYRILLIC CAPITAL LETTER ZHE

	0x87: 0x0417, // CYRILLIC CAPITAL LETTER ZE

	0x88: 0x0418, // CYRILLIC CAPITAL LETTER I

	0x89: 0x0419, // CYRILLIC CAPITAL LETTER SHORT I

	0x8a: 0x041a, // CYRILLIC CAPITAL LETTER KA

	0x8b: 0x041b, // CYRILLIC CAPITAL LETTER EL

	0x8c: 0x041c, // CYRILLIC CAPITAL LETTER EM

	0x8d: 0x041d, // CYRILLIC CAPITAL LETTER EN

	0x8e: 0x041e, // CYRILLIC CAPITAL LETTER O

	0x8f: 0x041f, // CYRILLIC CAPITAL LETTER PE

	0x90: 0x0420, // CYRILLIC CAPITAL LETTER ER

	0x91: 0x0421, // CYRILLIC CAPITAL LETTER ES

	0x92: 0x0422, // CYRILLIC CAPITAL LETTER TE

	0x93: 0x0423, // CYRILLIC CAPITAL LETTER U

	0x94: 0x0424, // CYRILLIC CAPITAL LETTER EF

	0x95: 0x0425, // CYRILLIC CAPITAL LETTER HA

	0x96: 0x0426, // CYRILLIC CAPITAL LETTER TSE

	0x97: 0x0427, // CYRILLIC CAPITAL LETTER CHE

	0x98: 0x0428, // CYRILLIC CAPITAL LETTER SHA

	0x99: 0x0429, // CYRILLIC CAPITAL LETTER SHCHA

	0x9a: 0x042a, // CYRILLIC CAPITAL LETTER HARD SIGN

	0x9b: 0x042b, // CYRILLIC CAPITAL LETTER YERU

	0x9c: 0x042c, // CYRILLIC CAPITAL LETTER SOFT SIGN

	0x9d: 0x042d, // CYRILLIC CAPITAL LETTER E

	0x9e: 0x042e, // CYRILLIC CAPITAL LETTER YU

	0x9f: 0x042f, // CYRILLIC CAPITAL LETTER YA

	0xa0: 0x0430, // CYRILLIC SMALL LETTER A

	0xa1: 0x0431, // CYRILLIC SMALL LETTER BE

	0xa2: 0x0432, // CYRILLIC SMALL LETTER VE

	0xa3: 0x0433, // CYRILLIC SMALL LETTER GHE

	0xa4: 0x0434, // CYRILLIC SMALL LETTER DE

	0xa5: 0x0435, // CYRILLIC SMALL LETTER IE

	0xa6: 0x0436, // CYRILLIC SMALL LETTER ZHE

	0xa7: 0x0437, // CYRILLIC SMALL LETTER ZE

	0xa8: 0x0438, // CYRILLIC SMALL LETTER I

	0xa9: 0x0439, // CYRILLIC SMALL LETTER SHORT I

	0xaa: 0x043a, // CYRILLIC SMALL LETTER KA

	0xab: 0x043b, // CYRILLIC SMALL LETTER EL

	0xac: 0x043c, // CYRILLIC SMALL LETTER EM

	0xad: 0x043d, // CYRILLIC SMALL LETTER EN

	0xae: 0x043e, // CYRILLIC SMALL LETTER O

	0xaf: 0x043f, // CYRILLIC SMALL LETTER PE

	0xb0: 0x2591, // LIGHT SHADE

	0xb1: 0x2592, // MEDIUM SHADE

	0xb2: 0x2593, // DARK SHADE

	0xb3: 0x2502, // BOX DRAWINGS LIGHT VERTICAL

	0xb4: 0x2524, // BOX DRAWINGS LIGHT VERTICAL AND LEFT

	0xb5: 0x2561, // BOX DRAWINGS VERTICAL SINGLE AND LEFT DOUBLE

	0xb6: 0x2562, // BOX DRAWINGS VERTICAL DOUBLE AND LEFT SINGLE

	0xb7: 0x2556, // BOX DRAWINGS DOWN DOUBLE AND LEFT SINGLE

	0xb8: 0x2555, // BOX DRAWINGS DOWN SINGLE AND LEFT DOUBLE

	0xb9: 0x2563, // BOX DRAWINGS DOUBLE VERTICAL AND LEFT

	0xba: 0x2551, // BOX DRAWINGS DOUBLE VERTICAL

	0xbb: 0x2557, // BOX DRAWINGS DOUBLE DOWN AND LEFT

	0xbc: 0x255d, // BOX DRAWINGS DOUBLE UP AND LEFT

	0xbd: 0x255c, // BOX DRAWINGS UP DOUBLE AND LEFT SINGLE

	0xbe: 0x255b, // BOX DRAWINGS UP SINGLE AND LEFT DOUBLE

	0xbf: 0x2510, // BOX DRAWINGS LIGHT DOWN AND LEFT

	0xc0: 0x2514, // BOX DRAWINGS LIGHT UP AND RIGHT

	0xc1: 0x2534, // BOX DRAWINGS LIGHT UP AND HORIZONTAL

	0xc2: 0x252c, // BOX DRAWINGS LIGHT DOWN AND HORIZONTAL

	0xc3: 0x251c, // BOX DRAWINGS LIGHT VERTICAL AND RIGHT

	0xc4: 0x2500, // BOX DRAWINGS LIGHT HORIZONTAL

	0xc5: 0x253c, // BOX DRAWINGS LIGHT VERTICAL AND HORIZONTAL

	0xc6: 0x255e, // BOX DRAWINGS VERTICAL SINGLE AND RIGHT DOUBLE

	0xc7: 0x255f, // BOX DRAWINGS VERTICAL DOUBLE AND RIGHT SINGLE

	0xc8: 0x255a, // BOX DRAWINGS DOUBLE UP AND RIGHT

	0xc9: 0x2554, // BOX DRAWINGS DOUBLE DOWN AND RIGHT

	0xca: 0x2569, // BOX DRAWINGS DOUBLE UP AND HORIZONTAL

	0xcb: 0x2566, // BOX DRAWINGS DOUBLE DOWN AND HORIZONTAL

	0xcc: 0x2560, // BOX DRAWINGS DOUBLE VERTICAL AND RIGHT

	0xcd: 0x2550, // BOX DRAWINGS DOUBLE HORIZONTAL

	0xce: 0x256c, // BOX DRAWINGS DOUBLE VERTICAL AND HORIZONTAL

	0xcf: 0x2567, // BOX DRAWINGS UP SINGLE AND HORIZONTAL DOUBLE

	0xd0: 0x2568, // BOX DRAWINGS UP DOUBLE AND HORIZONTAL SINGLE

	0xd1: 0x2564, // BOX DRAWINGS DOWN SINGLE AND HORIZONTAL DOUBLE

	0xd2: 0x2565, // BOX DRAWINGS DOWN DOUBLE AND HORIZONTAL SINGLE

	0xd3: 0x2559, // BOX DRAWINGS UP DOUBLE AND RIGHT SINGLE

	0xd4: 0x2558, // BOX DRAWINGS UP SINGLE AND RIGHT DOUBLE

	0xd5: 0x2552, // BOX DRAWINGS DOWN SINGLE AND RIGHT DOUBLE

	0xd6: 0x2553, // BOX DRAWINGS DOWN DOUBLE AND RIGHT SINGLE

	0xd7: 0x256b, // BOX DRAWINGS VERTICAL DOUBLE AND HORIZONTAL SINGLE

	0xd8: 0x256a, // BOX DRAWINGS VERTICAL SINGLE AND HORIZONTAL DOUBLE

	0xd9: 0x2518, // BOX DRAWINGS LIGHT UP AND LEFT

	0xda: 0x250c, // BOX DRAWINGS LIGHT DOWN AND RIGHT

	0xdb: 0x2588, // FULL BLOCK

	0xdc: 0x2584, // LOWER HALF BLOCK

	0xdd: 0x258c, // LEFT HALF BLOCK

	0xde: 0x2590, // RIGHT HALF BLOCK

	0xdf: 0x2580, // UPPER HALF BLOCK

	0xe0: 0x0440, // CYRILLIC SMALL LETTER ER

	0xe1: 0x0441, // CYRILLIC SMALL LETTER ES

	0xe2: 0x0442, // CYRILLIC SMALL LETTER TE

	0xe3: 0x0443, // CYRILLIC SMALL LETTER U

	0xe4: 0x0444, // CYRILLIC SMALL LETTER EF

	0xe5: 0x0445, // CYRILLIC SMALL LETTER HA

	0xe6: 0x0446, // CYRILLIC SMALL LETTER TSE

	0xe7: 0x0447, // CYRILLIC SMALL LETTER CHE

	0xe8: 0x0448, // CYRILLIC SMALL LETTER SHA

	0xe9: 0x0449, // CYRILLIC SMALL LETTER SHCHA

	0xea: 0x044a, // CYRILLIC SMALL LETTER HARD SIGN

	0xeb: 0x044b, // CYRILLIC SMALL LETTER YERU

	0xec: 0x044c, // CYRILLIC SMALL LETTER SOFT SIGN

	0xed: 0x044d, // CYRILLIC SMALL LETTER E

	0xee: 0x044e, // CYRILLIC SMALL LETTER YU

	0xef: 0x044f, // CYRILLIC SMALL LETTER YA

	0xf0: 0x0401, // CYRILLIC CAPITAL LETTER IO

	0xf1: 0x0451, // CYRILLIC SMALL LETTER IO

	0xf2: 0x0404, // CYRILLIC CAPITAL LETTER UKRAINIAN IE

	0xf3: 0x0454, // CYRILLIC SMALL LETTER UKRAINIAN IE

	0xf4: 0x0407, // CYRILLIC CAPITAL LETTER YI

	0xf5: 0x0457, // CYRILLIC SMALL LETTER YI

	0xf6: 0x040e, // CYRILLIC CAPITAL LETTER SHORT U

	0xf7: 0x045e, // CYRILLIC SMALL LETTER SHORT U

	0xf8: 0x00b0, // DEGREE SIGN

	0xf9: 0x2219, // BULLET OPERATOR

	0xfa: 0x00b7, // MIDDLE DOT

	0xfb: 0x221a, // SQUARE ROOT

	0xfc: 0x2116, // NUMERO SIGN

	0xfd: 0x00a4, // CURRENCY SIGN

	0xfe: 0x25a0, // BLACK SQUARE

	0xff: 0x00a0, // NO-BREAK SPACE
}

var registersDescription = map[byte]string{
	0x01: "Сумма регистраций",
	0x03: "Сумма платежей",
	0x04: "Сумма внесений",
	0x05: "Сумма выплат",
	0x06: "Количество регистраций",
	0x08: "Количество внесений",
	0x09: "Количество выплат",
	0x0A: "Наличность в кассе",
	0x0B: "Выручка",
	0x0C: "Сменный итог",
	0x11: "Текущая дата и время",
	0x12: "Смена",
	0x13: "Статус",
	0x14: "Сумма, остаток, сдача по чеку",
	0x15: "Номер смены",
	0x16: "Заводской номер",
	0x17: "Модель, версия, подверсия ККТ",
	0x18: "Ширина чековой ленты",
	0x19: "Максимальный размер пакета в байтах",
	0x1A: "Номер и количество оставшихся перерегистраций",
	0x1B: "Дата регистрации/перерегистрации",
	0x28: "Необнуляемая сумма",
	0x2A: "Сумма скидок и надбавок за смену",
	0x2B: "Код ошибки обмена ОФД",
	0x2C: "Количество неотправленных документов в ФН",
	0x2D: "Дата самого раннего неотправленного документа в ФН",
	0x2E: "Сумма аннулирований в смене",
	0x2F: "Номер и состояние ФН",
	0x30: "Номер ФД последней регистрации/перерегистрации и дата окончания срока действия ФН",
	0x33: "Номер ФД чека, тип чека, итог, дата и время чека и фискальный признак",
	0x34: "Номер, фискальный признак, дата и время последнего ФД",
	0x35: "Количество чеков за смену и номер смены",
	0x36: "Версия ФФД ККТ, ФФД ФН, ФФД",
	0x38: "Необнуляемая сумма по типам оплаты",
	0x39: "Дата и время отправки последнего документа в ОФД",
	0x3A: "Общий счетчик количества расчетных документов с момента общего гашения",
}
